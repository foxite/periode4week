﻿using UnityEngine;

// This one should be placed on the camera, but theoretically it should work everywhere
public class BombButtonClick : MonoBehaviour
{
	private Camera m_Camera;

	private void Start()
	{
		m_Camera = Camera.main;
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			// Get object that mouse was over
			Ray mouseRay = m_Camera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			if (Physics.Raycast(mouseRay, out hitInfo))
			{
				// Was it a button?
				BombButton button = hitInfo.transform.GetComponent<BombButton>();
				if (button != null)
				{
					// Press that button
					button.m_Bomb.PressButton(button.m_Key);
				}
			}
		}
	}
}
