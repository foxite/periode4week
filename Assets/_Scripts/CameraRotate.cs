﻿using UnityEngine;

public class CameraRotate : MonoBehaviour
{
	[SerializeField] private float m_MaxTurn = 36;

	private Vector3 m_OriginalRotation;

	private void Start()
	{
		m_OriginalRotation = transform.localRotation.eulerAngles;
	}

	private void Update()
	{
		if (Input.mousePosition.x > 0 && Input.mousePosition.x < Screen.width &&
			Input.mousePosition.y > 0 && Input.mousePosition.y < Screen.height)
		{
			float yRotation = (Input.mousePosition.x - Screen.width / 2) * m_MaxTurn / Screen.width;
			float xRotation = (Input.mousePosition.y - Screen.height / 2) * m_MaxTurn / Screen.height;
			transform.localRotation = Quaternion.Euler(m_OriginalRotation.x - xRotation, m_OriginalRotation.y + yRotation, m_OriginalRotation.z);
		}
	}
}
