﻿#pragma warning disable 0649
using System.Collections;
using UnityEngine;

public class BombActions : MonoBehaviour
{
	// References
	[SerializeField] private Radio m_Radio;
	[SerializeField] private AudioClip[] m_ButtonPressSounds;
	[SerializeField] private AudioClip m_IncorrectSound;
	private AudioSource m_AS1, m_AS2;
	private BombTimer m_Timer;

	// Settings
	[SerializeField] private int m_ActionsToWin; // How many actions must be done to defuse the bomb
	[SerializeField] private string[] m_Keys;

	// State variables
	private int m_NumberOfActions;
	private float m_TimeForAction;
	private string m_ButtonToPress;

	private void Start()
	{
		m_Timer = GetComponentInChildren<BombTimer>();
		m_AS1 = GetComponents<AudioSource>()[0];
		m_AS2 = GetComponents<AudioSource>()[1];

		m_NumberOfActions = 0;

		StartCoroutine(StartAction(2.0f));
	}

	private void Update()
	{
		if (PauseManager.Instance.Paused || m_Timer.m_State != BombState.Armed)
			return;

		if (m_TimeForAction > 0)
		{
			m_TimeForAction -= Time.deltaTime;
		}
		
		if (m_TimeForAction <= 0)
		{
			NextAction(false);
		}
	}

	// This is used in Start() to delay the start of the game long enough to play the begin message
	private IEnumerator StartAction(float delay)
	{
		yield return new WaitForSeconds(delay);
		if (m_Radio != null)
			m_Radio.PlayBeginMessage();
		yield return new WaitForSeconds(2f);

		m_Timer.m_State = BombState.Armed;
		NextAction(false);
	}

	/// <summary>
	/// Randomly generates a new action.
	/// </summary>
	/// <param name="shouldIncrement">True if Simon said to press a key, and the user did it. False for anything else</param>
	private void NextAction(bool shouldIncrement)
	{
		if (shouldIncrement)
			m_NumberOfActions++;

		if (m_NumberOfActions > m_ActionsToWin)
		{
			m_Timer.Defuse();
		}
		else
		{
			m_ButtonToPress = m_Keys[Random.Range(0, m_Keys.Length)];
			m_TimeForAction = 3f;
			if (m_Radio != null)
				StartCoroutine(m_Radio.PlayKeyMessage(m_ButtonToPress));
		}
	}

	/// <summary>
	/// Should only be used by UI buttons, not by other scripts
	/// </summary>
	/// <param name="number">Number of button to press (0-indexed)</param>
	public void PressButton(string key)
	{
		if (PauseManager.Instance.Paused) // We don't want to sound button presses while paused,
			return;

		m_AS1.clip = m_ButtonPressSounds[Random.Range(0, m_ButtonPressSounds.Length)];
		m_AS1.Play();

		if (m_Timer.m_State != BombState.Armed) // But we do after the bomb was disarmed (or exploded, but we'd be in the game over screen)
			return;
		
		if (m_ButtonToPress == key)
		{
			// He pressed the right button
			//m_Timer.Bonus(2.5f);
			NextAction(true);
		}
		else
		{
			// He pressed the wrong button
			m_Timer.Penalty(3.0f);
			m_AS2.clip = m_IncorrectSound;
			m_AS2.Play();
			NextAction(false);
		}
	}
}
