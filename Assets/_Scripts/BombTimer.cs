﻿#pragma warning disable 0649
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class BombTimer : MonoBehaviour
{
	// References
	[SerializeField] private Text m_TimeText;
	[SerializeField] private AudioClip m_Sub20SecSound, m_GameOverSound;
	private AudioSource m_AS;

	// Settings
	[SerializeField] private float m_TimerStart;
	[SerializeField] private string m_NextLevel;

	// State variables
	private float m_CurrentTimerProgress;
	private bool m_Sub20Sec = false;
	public BombState m_State;

	// Events

	private void Start()
	{
		m_AS = GetComponent<AudioSource>();
		m_CurrentTimerProgress = m_TimerStart;
		m_State = BombState.NotStarted;
		StartCoroutine(FadeScreen(false, 1.5f));
	}

	private void Update()
	{
		if (PauseManager.Instance.Paused || m_State != BombState.Armed)
			return;

		if (m_CurrentTimerProgress > 0)
		{
			m_CurrentTimerProgress -= Time.deltaTime;

			if (m_CurrentTimerProgress < 20 && !m_Sub20Sec)
			{
				m_Sub20Sec = true;
				m_AS.time = 0f;
				m_AS.clip = m_Sub20SecSound;
				m_AS.Play();
			}

			TimeSpan timeLeft = TimeSpan.FromSeconds(m_CurrentTimerProgress);
			string timeRemaining = string.Format("{0:D2}:{1:D2}",
				timeLeft.Minutes,
				timeLeft.Seconds);
			m_TimeText.text = timeRemaining;

			if (m_CurrentTimerProgress < 0)
			{
				Explode();
			}
		}
	}

	// Game over
	public void Explode()
	{
		m_AS.Stop();
		m_AS.clip = m_GameOverSound;
		m_AS.Play();
		m_State = BombState.Exploded;
		m_TimeText.text = "--:--";

		Image flashPanel = GameObject.Find("FlashPanel").GetComponent<Image>();
		flashPanel.color = new Color(1f, 1f, 1f, 1f);
		
		StartCoroutine(LoadSceneFade("GameOver", 10.0f, 1.5f));
	}

	public void Defuse()
	{
		m_AS.Stop();
		m_State = BombState.Defused;
		m_TimeText.text = "DISARMED";

		StartCoroutine(LoadSceneFade(m_NextLevel, 2.0f, 1.5f));
	}
	
	/// <param name="delay">Delay in seconds</param>
	/// <param name="fadeTime">Fade time in seconds</param>
	private IEnumerator LoadSceneFade(string sceneName, float delay, float fadeTime)
	{
		yield return new WaitForSeconds(delay);
		yield return FadeScreen(true, fadeTime);
		SceneManager.LoadScene(sceneName); // The new BombTimer will fade the screen from black
	}
	
	/// <param name="toBlack">False to fade from black</param>
	private IEnumerator FadeScreen(bool toBlack, float fadeTime)
	{
		Image panel = GameObject.Find("FadePanel").GetComponent<Image>();
		for (float t = 0; t < fadeTime; t += Time.deltaTime)
		{
			if (toBlack)
			{
				panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, (t / fadeTime));
			}
			else
			{
				panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 1f - (t / fadeTime));
			}
			yield return new WaitForEndOfFrame();
		}
		if (toBlack)
			panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 1f);
		else
			panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0f);
	}

	// Decreases time to detonation.
	public void Penalty(float seconds)
	{
		if (PauseManager.Instance.Paused)
			Debug.LogWarning("A penalty was incurred while paused.");

		m_CurrentTimerProgress -= seconds;
		if (m_CurrentTimerProgress < 20)
		{
			m_Sub20Sec = true;
			m_AS.time = 20 - m_CurrentTimerProgress;
		}
	}

	// Increases time to detonation.
	public void Bonus(float seconds)
	{
		if (PauseManager.Instance.Paused)
			Debug.LogWarning("A bonus was incurred while paused.");

		m_CurrentTimerProgress += seconds;
		if (m_CurrentTimerProgress >= 20 && m_Sub20Sec)
		{
			m_Sub20Sec = false;
			m_AS.Stop();
		}
	}
}

public enum BombState { NotStarted, Armed, Defused, Exploded }
