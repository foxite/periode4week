﻿using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
	[SerializeField]
	public Slider m_VolumeSlider;

	public void VolumeSlider()
	{
		AudioListener.volume = m_VolumeSlider.value;
	}
}
