﻿using System;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
	public static PauseManager Instance { get; private set; }

	public bool Paused { get; private set; }

	public event EventHandler OnPause;
	public event EventHandler OnUnpause;

	private GameObject m_PausePanel;

	private void Awake()
	{
		Instance = this;
	}

	private void Start()
	{
		m_PausePanel = GameObject.FindGameObjectWithTag("PausePanel");
		m_PausePanel.SetActive(false); // If we set it inactive in the editor, it would not be found by the above line
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.P))
		{
			Paused = !Paused;
			if (Paused)
			{
				m_PausePanel.SetActive(true);
				if (OnPause != null)
				{
					OnPause(this, null);
				}
			}
			else
			{
				m_PausePanel.gameObject.SetActive(false);
				if (OnUnpause != null)
				{
					OnUnpause(this, null);
				}
			}
		}
	}
}
