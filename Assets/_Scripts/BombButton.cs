﻿using UnityEngine;

// Does nothing, this just carries the button number
public class BombButton : MonoBehaviour {
	public string m_Key;
	public BombActions m_Bomb;

	private void Start()
	{
		m_Bomb = GetComponentInParent<BombActions>();
	}
}
