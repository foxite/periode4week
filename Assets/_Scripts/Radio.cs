﻿#pragma warning disable 0649
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Radio : MonoBehaviour
{
	// this is ugly, but this is a gamejam. Get it done fast, not beautifully.
	[SerializeField] private AudioClip[] m_Asterisk, m_Hashtag, m_Zero, m_One, m_Two, m_Three, m_Four, m_Five, m_Six, m_Seven, m_Eight, m_Nine;
	[SerializeField] private AudioClip m_Start, m_End, m_Begin;

	private Dictionary<string, AudioClip[]> m_Messages;

	private AudioSource m_AS;

	private void Start()
	{
		m_AS = GetComponent<AudioSource>();

		m_Messages = new Dictionary<string, AudioClip[]>()
		{
			{ "*", m_Asterisk },
			{ "#", m_Hashtag },
			{ "0", m_Zero },
			{ "1", m_One },
			{ "2", m_Two },
			{ "3", m_Three },
			{ "4", m_Four },
			{ "5", m_Five },
			{ "6", m_Six },
			{ "7", m_Seven },
			{ "8", m_Eight },
			{ "9", m_Nine },
		};
	}

	public System.Collections.IEnumerator PlayKeyMessage(string key)
	{
		m_AS.clip = m_Start;
		m_AS.Play();
		yield return new WaitUntil(() => !m_AS.isPlaying);
		AudioClip[] messages = m_Messages[key];
		int index = Random.Range(0, messages.Length);
		m_AS.clip = messages[index];
		m_AS.Play();
		yield return new WaitUntil(() => !m_AS.isPlaying);
		m_AS.clip = m_End;
		m_AS.Play();
	}

	public void PlayBeginMessage()
	{
		m_AS.clip = m_Begin;
		m_AS.Play();
	}
}
